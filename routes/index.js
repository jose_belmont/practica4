const express = require('express');
let router = express.Router();
let Tipo = require('../models/tipo');

router.get('/', (req, res) => {
    Tipo.find().then( resultado => {
        res.render(__dirname + '/../views/index', {tipos:resultado});
        
    })
});

router.get('/nuevo_inmueble', (req, res) => {
    Tipo.find().then(resultado => {
        console.log(resultado);
        res.render(__dirname + '/../views/nuevo_inmueble', { tipos: resultado });
    }).catch(error => {
        res.render(__dirname + '/../views/nuevo_inmueble', { tipos: [] })
    });
});

// 2.2.2 Nuevas rutas enrutador principal
router.get("/login", (req, res) => {
    Tipo.find().then(resultado => {
        console.log(resultado);
        res.render(__dirname + '/../views/login', { tipos: resultado });
    }).catch(error => {
        res.render(__dirname + '/../views/login', { tipos: [] })
    });
});

router.get("/registro", (req, res) => {
    Tipo.find().then(resultado => {
        console.log(resultado);
        res.render(__dirname + '/../views/registro', { tipos: resultado });
    }).catch(error => {
        res.render(__dirname + '/../views/registro', { tipos: [] })
    });
});
module.exports = router;