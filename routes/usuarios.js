const express = require('express');
const fileUpload = require('express-fileupload');
const mongoose = require('mongoose');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
let fs = require('fs');
let Inmueble = require(__dirname + '/../models/inmueble.js');
let Tipo = require(__dirname + '/../models/tipo');

let router = express.Router();

let Usuario = require(__dirname + '/../models/usuario');


//palabra secreta
const secreto = "secreto";
let generarToken = id => {
    return jwt.sign({
        id: id
    }, secreto, {
        expiresIn: "1 year"
    });
}

//2.2.1 enrutador de usuarios

//registro de usuario
router.post('/registro', (req, res) => {

    let nuevoUsuario = new Usuario({
        nombre: req.body.nombre,
        login: req.body.login,
        password: md5(req.body.password)
    });
    
    nuevoUsuario.save().then(resultado => {
        Tipo.find().then(result => {
            res.render(__dirname + '/../views/login', {
                inmueble: resultado,
                tipos: result
            });
        });
    }).catch(err => {
        // console.log("error:",err);
        res.render(__dirname + '/../views/registro', {
            error: true,
            mensError: err
        });
    });
});

//validar usuario
router.post('/login', (req, res) => {
    console.log(req.body);
    Usuario.findOne({
            login: req.body.login,
            password: md5(req.body.password)
        })
        .then(resultado => {
            console.log('resultado: ', resultado);
            if (resultado) {
                res.send({
                    ok: true,
                    token: generarToken(resultado._id),
                    newLocation: '/'
                });
            } else {
                res.send({
                    ok: false,
                    mensError: "Error en los datos"
                });
            }
        }).catch(error => {
            res.send({
                ok: false,
                mensError: "No existe el usuario"
            });
        });
});

module.exports = router;