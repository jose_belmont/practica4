function borrar($id){
    //alert($id);
    $.ajax(
        {
            url:"/inmuebles/" + $id, 
            type: "DELETE",
            data: JSON.stringify({}),
            contentType:"aplication/json; charset=utf-8",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
            },
            success: () => {
                document.location.href="/inmuebles";
            }
        }
    );
}

function submitLogin() {
    let data = {
        login: document.getElementById('login').value,
        password: document.getElementById('password').value
    };

    let peticion = new XMLHttpRequest();

    peticion.open('POST', '/usuarios/login', true);
    peticion.setRequestHeader('Content-type', 'application/json');
    console.log('objeto stringify: ', JSON.stringify(data));
    peticion.send(JSON.stringify(data));

    peticion.onreadystatechange = event => {
        if (peticion.readyState == 4 && peticion.status == 200) {
            let respuesta = JSON.parse(peticion.responseText);

            if (respuesta.ok) {
                localStorage.setItem('token', respuesta.token);
                window.location.href = "/";
            } else {
                let alert = document.getElementById('errormsg');
                alert.classList.add('alert-danger');
                alert.innerHTML = respuesta.error;
            }
        }
    }
}

function submitCreate() {
    var formData = new FormData(document.getElementById('form-nuevo_inm'));

    $("#form-nuevo_inm").submit(function (e) {
        e.preventDefault();
    });
    $.ajax({
        url: "/inmuebles",
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
        },
        success: data => {
            console.log(data);
            $('#contenido').html(data);
            location.href = data.newLocation;
        }
    });

}