const express = require('express');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const passport = require('passport');

//poner el __dirname para no tener problemas con las rutas en el vps
const index = require(__dirname +'/routes/index');
const tipos = require(__dirname +'/routes/tipos');
const inmuebles = require(__dirname + '/routes/inmuebles');
const usuarios = require(__dirname + '/routes/usuarios');


const secreto = 'secreto';

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/inmuebles');

let validarToken = (token) => {
    try {
        let resultado = jwt.verify(token, secreto);
        return resultado;
    } catch (e) {
        console.log(e);
    }
}

let app = express();

app.use(passport.initialize());

// para el motor de plantillas
app.set('view engine', 'ejs');

//Parsea a json las peticiones
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(fileUpload());

//para los archivos estaticos
app.use('/', express.static(__dirname + '/public'));

app.use('/', index);
app.use('/inmuebles', inmuebles);
app.use('/tipos', tipos);
app.use('/usuarios', usuarios);

// app.use((req, res, next) => {
//     res.status(404);
//     res.render('404', { url: req.url });
// });

//para local
//app.listen(3030);

//para manager en vps
module.exports.app = app;